"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.gif = void 0;
const axios_1 = __importDefault(require("axios"));
const config_json_1 = require("../config/config.json");
function gif(message, args) {
    return __awaiter(this, void 0, void 0, function* () {
        const url = config_json_1.tenor.urlSearch;
        console.log(args);
        const params = {
            key: process.env.KEY_TENOR,
            q: args.join(' ') || 'explotion'
        };
        try {
            console.log(params);
            let result = yield axios_1.default.get(url, { params });
            if (!result.data.results.length) {
                params.q = 'random';
                result = yield axios_1.default.get(url, { params });
            }
            const data = result.data;
            sendGif(message, data);
        }
        catch (error) {
            console.log('Este es el error papu:', error);
        }
    });
}
exports.gif = gif;
function sendGif(message, data) {
    const i = Math.floor(Math.random() * data.results.length);
    message.channel.send(data.results[i].url);
}
