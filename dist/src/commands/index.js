"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const help_1 = require("./help");
const gif_1 = require("./gif");
const p_1 = require("./p");
const prefix_json_1 = require("../utils/prefix.json");
const commands = {
    help: help_1.help,
    gif: gif_1.gif,
    p: p_1.p
};
function default_1(message) {
    return __awaiter(this, void 0, void 0, function* () {
        console.log(`${message.channel.id}`);
        console.log('Este es el mensaje:', message.content);
        const tokens = message.content.split(' ');
        let command = tokens.shift();
        if (message.author.id === '751629309411590255') {
            message.reply('Hola hermosa, en este momento no hay muchos comandos, pero ya habrá.');
            return;
        }
        if (command.startsWith(prefix_json_1.prefix)) {
            command = command.substring(1);
            if (!commands[command])
                return message.reply('Este comando no existe papu');
            commands[command](message, tokens);
        }
        if (message.content === 'Vete a la mierda') {
            message.reply('Tu vete a la mierda');
            return;
        }
        if (message.content === 'GTFO') {
            message.channel.send('Nos vemos people !');
            return;
        }
    });
}
exports.default = default_1;
