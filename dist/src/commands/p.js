"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.p = void 0;
const axios_1 = __importDefault(require("axios"));
require("ffmpeg-static");
require("@discordjs/opus");
const ytdl_core_1 = __importDefault(require("ytdl-core"));
const voice_1 = require("@discordjs/voice");
const config_json_1 = require("../config/config.json");
function p(message, args) {
    return __awaiter(this, void 0, void 0, function* () {
        console.log(message);
        console.log('Entro a play music !');
        console.log('Canal de voz', message.member.voice.channelId);
        if (!message.member.voice.channelId) {
            message.reply('Debes estar en un canal de audio');
            return;
        }
        console.log('Permisos', message.member.voice.channel.permissionsFor(message.client.user));
        const params = {
            key: process.env.KEY_YOUTUBE,
            q: args.join(' ') || 'explotion',
            type: 'video'
        };
        const { data } = yield axios_1.default.get(config_json_1.youtube.urlApiSearch, { params });
        const { items } = data;
        if (!items)
            return message.reply('No existe ningun video asi culia');
        const videoId = items[0].id.videoId;
        const url = config_json_1.youtube.urlVideo.replace(':videoId', videoId);
        const songData = yield ytdl_core_1.default.getInfo(url);
        const song = {
            title: songData.videoDetails.title
        };
        console.log(message.member.voice.channelId, 'El channel');
        const VoiceConnection = (0, voice_1.joinVoiceChannel)({
            channelId: message.member.voice.channelId,
            guildId: message.guild.id,
            adapterCreator: message.guild.voiceAdapterCreator
        });
        const player = (0, voice_1.createAudioPlayer)();
        const stream = (0, ytdl_core_1.default)(url, { filter: 'audioonly' });
        const resource = (0, voice_1.createAudioResource)(stream, {
            inputType: voice_1.StreamType.Arbitrary
        });
        VoiceConnection.subscribe(player);
        player.play(resource);
        console.log('resource', resource);
        message.channel.send(`Suena [${song.title}](${url}). A disfrutar !`);
        console.log(url);
    });
}
exports.p = p;
