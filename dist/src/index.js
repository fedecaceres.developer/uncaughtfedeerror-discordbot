"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const dotenv_1 = require("dotenv");
(0, dotenv_1.config)();
const discord_js_1 = require("discord.js");
const index_1 = __importDefault(require("./commands/index"));
const token = process.env.TOKEN_DISCORD;
const client = new discord_js_1.Client({
    intents: [discord_js_1.Intents.FLAGS.GUILDS, discord_js_1.Intents.FLAGS.GUILD_MESSAGES, discord_js_1.Intents.FLAGS.GUILD_VOICE_STATES]
});
exports.default = client;
client.on('ready', () => {
    console.log('Ready perro!');
    client.user.setStatus('online');
});
client.on('messageCreate', (message) => {
    (0, index_1.default)(message);
});
client.login(token);
