const { REST } = require("@discordjs/rest");
const { Routes } = require("discord-api-types/v9");
const ping = require("./commands/ping");
const server = require("./commands/server");
const commands = [ping, server].map((command) => command.toJSON());
const rest = new REST({ version: "9" }).setToken(token);
rest
    .put(Routes.applicationGuildCommands(clientId, guildId), { body: commands })
    .then(() => console.log("Successfully registered application commands."))
    .catch(console.error);
