import { config } from 'dotenv'
config()

import { Client, Intents, Message } from 'discord.js'
import commandHandler from './commands/index'
const token = process.env.TOKEN_DISCORD

// Create a new client instance
const client: Client = new Client({
  intents: [Intents.FLAGS.GUILDS, Intents.FLAGS.GUILD_MESSAGES, Intents.FLAGS.GUILD_VOICE_STATES]
})

export default client

// When the client is ready, run this code (only once)
client.on('ready', () => {
  console.log('Ready perro!')
  client.user.setStatus('online')
})

client.on('messageCreate', (message: Message) => {
  commandHandler(message)
})

// client.on("guildMemberAdd", (member) => {
//   let canal = client.channels.cache.get("ID-CANAL");
//   canal.send(
//     `Hola ${member.user}, bienvenido al servidor ${member.guild.name} pasala bien!.`
//   );
// });

// crear objeto

// Login to Discord with your client's token
client.login(token)
