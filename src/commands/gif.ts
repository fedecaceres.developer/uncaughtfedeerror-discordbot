import axios from 'axios'
import { Message } from 'discord.js'
import { tenor } from '../config/config.json'

async function gif(message: Message, args: string[]) {
  const url = tenor.urlSearch
  console.log(args)
  const params = {
    key: process.env.KEY_TENOR,
    q: args.join(' ') || 'explotion'
  }

  try {
    console.log(params)
    let result = await axios.get(url, { params })
    if (!result.data.results.length) {
      params.q = 'random'
      result = await axios.get(url, { params })
    }
    const data = result.data
    sendGif(message, data)
  } catch (error) {
    console.log('Este es el error papu:', error)
  }
}

function sendGif(message, data) {
  const i = Math.floor(Math.random() * data.results.length)
  message.channel.send(data.results[i].url)
}

export { gif }
