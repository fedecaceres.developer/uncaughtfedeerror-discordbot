import axios from 'axios'
import 'ffmpeg-static'
import '@discordjs/opus'
import ytdl from 'ytdl-core'

import {
  joinVoiceChannel,
  createAudioPlayer,
  createAudioResource,
  StreamType,
  DiscordGatewayAdapterCreator
} from '@discordjs/voice'

// const Commando = require("discord.js-comando");

import { youtube } from '../config/config.json'
import { Message } from 'discord.js'

async function p(message: Message, args: string[]) {
  console.log(message)
  console.log('Entro a play music !')

  console.log('Canal de voz', message.member.voice.channelId)
  if (!message.member.voice.channelId) {
    message.reply('Debes estar en un canal de audio')
    return
  }

  console.log('Permisos', message.member.voice.channel.permissionsFor(message.client.user))

  const params = {
    key: process.env.KEY_YOUTUBE,
    q: args.join(' ') || 'explotion',
    type: 'video'
  }
  const { data } = await axios.get(youtube.urlApiSearch, { params })
  const { items } = data

  if (!items) return message.reply('No existe ningun video asi culia')

  const videoId = items[0].id.videoId
  const url = youtube.urlVideo.replace(':videoId', videoId)

  const songData = await ytdl.getInfo(url)
  const song = {
    title: songData.videoDetails.title
  }
  console.log(message.member.voice.channelId, 'El channel')

  //  DiscordGatewayAdapterCreator

  // const mappedVoiceAdaptedCreator: DiscordGatewayAdapterCreator = ())

  const VoiceConnection = joinVoiceChannel({
    channelId: message.member.voice.channelId,
    guildId: message.guild.id,
    adapterCreator: message.guild.voiceAdapterCreator as unknown as DiscordGatewayAdapterCreator
  })

  const player = createAudioPlayer()

  const stream = ytdl(url, { filter: 'audioonly' })
  const resource = createAudioResource(stream, {
    inputType: StreamType.Arbitrary
  })

  VoiceConnection.subscribe(player)

  player.play(resource)
  console.log('resource', resource)

  // console.log("resource", resource);

  // VoiceConnection.subscribe(player);
  // VoiceConnection.play();

  // player.on("idle", () => {
  //   try {
  //     player.stop();
  //   } catch (e) {}
  //   try {
  //     VoiceConnection.destroy();
  //   } catch (e) {}
  //   joinChannel(message.member.voice.channel.id);
  // });

  // const connection =
  //   await message.member.voice.channel.JoinVoiceChannelOptions();

  // const trigger = await connection.play(ytdl(url), {
  //   filter: "audioonly",
  //   quality: "highest",
  // });

  message.channel.send(`Suena [${song.title}](${url}). A disfrutar !`)

  console.log(url)
}

export { p }
