import { help } from './help'
import { gif } from './gif'
import { p } from './p'
import { Message } from 'discord.js'
import { prefix } from '../utils/prefix.json'

const commands = {
  help,
  gif,
  p
}

export default async function (message: Message) {
  console.log(`${message.channel.id}`)
  console.log('Este es el mensaje:', message.content)

  // Divido el comando de los argumentos en un array
  const tokens = message.content.split(' ')

  // elimino el comando del array, y lo guardo en command variable
  let command = tokens.shift()

  if (message.author.id === '751629309411590255') {
    message.reply('Hola hermosa, en este momento no hay muchos comandos, pero ya habrá.')
    return
  }
  // if (message.author.id === "290190836447182848") {
  //   message.reply(
  //     "Hola Markitoo, en este momento no hay muchos comandos, pero ya habrá. Disfruta este server"
  //   );
  //   return;
  // }

  // if (message.author.id === "189139683979034624") {
  //   message.reply(
  //     "Hola Veriii. Primero, LOS PELUCAS SABEN. Segundo, todavia no tengo comandos. VERI GATO"
  //   );
  //   return;
  // }

  // if (command.charAt(0) === '!') {

  if (command.startsWith(prefix)) {
    // Tomo la cadena que esta adelante del !
    command = command.substring(1)

    if (!commands[command]) return message.reply('Este comando no existe papu')

    commands[command](message, tokens)
  }

  if (message.content === 'Vete a la mierda') {
    message.reply('Tu vete a la mierda')
    return
  }

  if (message.content === 'GTFO') {
    message.channel.send('Nos vemos people !')
    return
  }

  //   if (message.content === "!help") {
  //     message.channel.send({ embeds: [helpEmbed()] });
  //   }
  // message.reply(
  //   `Holaa soy el bot, te saludo, pronto tendremos mas comandos !s`
  // );
}
